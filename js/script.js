'use strict'

//Scrool down function
const links = document.querySelectorAll('header li a');
for(let link of links){
    link.addEventListener('click',clickHandler);
}

function clickHandler(e){
    //Navigacija unutar stranice
    e.preventDefault();
    const href = this.getAttribute('data-link');
    if(href != null){
        const offsetTop = document.querySelector(href).offsetTop;

        scroll({
            top:offsetTop,
            behavior:'smooth'
        });
    }
}

const shop_lightbox = document.querySelector('.shop-lightbox');
const nav_shop_btn = document.querySelector('header li a[data-name="shop"]');

function shopActive(){
    shop_lightbox.classList.add('shop-active');
}
function closeShop(){
    shop_lightbox.classList.remove('shop-active');
}
nav_shop_btn.addEventListener('click',shopActive);

const in_shop_btn = document.querySelector('.shop-lightbox p.goback').addEventListener("click",closeShop);










function responsive_menu_f(){
    responsive_menu.classList.toggle('res_active');
}

const res_links = document.querySelectorAll('.responsive-menu ul li a');
for(let link of res_links){
    link.addEventListener('click',clickHandler);
    
}

const responsive_menu = document.querySelector('.responsive-menu');



let responsive_close = document.querySelector('.responsive-menu p.close-res').addEventListener('click',responsive_menu_f);
let responsive_icon = document.querySelector('header img.responsive-icon').addEventListener('click',responsive_menu_f);
